﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using EmployeeManagement.Models;
using EmployeeManagement.ViewModels;


namespace EmployeeManagement.Controllers
{
    public class ErrorController : Controller
    {
        [Route("Error/{statusCode}")]
        public IActionResult HttpStatusCodeHandler(int statusCode)
        {
            var statusCodeResult = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();
            if (statusCodeResult != null)
            {
                var httpStatusCode = new HttpStatusCode();
                switch (statusCode)
                {
                    case 404:
                        httpStatusCode.ErrorMessage = "Sorry, the resource you requested could not be found";
                        httpStatusCode.Path = statusCodeResult.OriginalPath;
                        httpStatusCode.QS = statusCodeResult.OriginalQueryString;
                        break;
                }
                return View("NotFound");
            }
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        [Route("Error")]
        public IActionResult Error()
        {
            // Retrieve the exception Details
            var exceptionHandlerPathFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();

            if (exceptionHandlerPathFeature != null)
            {
                ErrorViewModel errorVM = new ErrorViewModel
                {
                    ErrorMessage = new ErrorMessage
                    {
                        ExceptionPath = exceptionHandlerPathFeature.Path,
                        ExceptionMessage = exceptionHandlerPathFeature.Error.Message,
                        StackTrace = exceptionHandlerPathFeature.Error.StackTrace
                    }
                };
                return View("Error");
            }

            return RedirectToAction("Index", "Home");
        }
    }
}
