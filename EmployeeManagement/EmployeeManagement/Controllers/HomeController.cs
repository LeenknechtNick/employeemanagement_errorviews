﻿using System;
using System.IO;
using EmployeeManagement.DataAccessLayer.Models;
using EmployeeManagement.DataAccessLayer.Repositories;
using EmployeeManagement.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace EmployeeManagement.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public HomeController(
            IEmployeeRepository employeeRepository,
            IWebHostEnvironment webHostEnvironment)
        {
            _employeeRepository = employeeRepository;
            _webHostEnvironment = webHostEnvironment;
        }

        public IActionResult Index()
        {
            var employeeList = _employeeRepository.GetAll();
            return View(employeeList);
        }

        public IActionResult Details(int? id)
        {
            //throw new Exception("Error in Details View");

            var employee = _employeeRepository.GetById(id ?? 1);

            if (employee == null)
            {
                Response.StatusCode = 404;
                return View("EmployeeNotFound", id);
            }

            HomeDetailsViewModel homeDetailsViewModel = new HomeDetailsViewModel()
            {
                Employee = employee,
                PageTitle = "Employee Details"
            };

            return View(homeDetailsViewModel);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(EmployeeCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                string uniqueFileName = ProcessUploadedFile(model);

                var newEmployee = new Employee
                {
                    Name = model.Name,
                    Email = model.Email,
                    Department = model.Department,
                    BankAccountNumber = model.BankAccountNumber,
                    PhotoPath = uniqueFileName
                };

                var response = _employeeRepository.Add(newEmployee);

                if (response != null && response.Id != 0)
                {
                    return RedirectToAction("Details", new { id = response.Id });
                }
            }

            return View();
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {  
            var employee = _employeeRepository.GetById(id);
            if (employee != null)
            {
                var employeeEditVM = new EmployeeEditViewModel()
                {
                    Id = employee.Id,
                    Name = employee.Name,
                    BankAccountNumber = employee.BankAccountNumber,
                    Department = employee.Department,
                    Email = employee.Email,
                    ExistingPhotoPath = employee.PhotoPath,
                };

                return View(employeeEditVM);
            }
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public IActionResult Edit(EmployeeEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                var employee = _employeeRepository.GetById(model.Id);

                employee.Name = model.Name;
                employee.Email = model.Email;
                employee.Department = model.Department;
                employee.BankAccountNumber = model.BankAccountNumber;

                if (model.Photo != null)
                {
                    if (model.ExistingPhotoPath != null)
                    {
                        var filePath = Path.Combine(_webHostEnvironment.WebRootPath, "images", model.ExistingPhotoPath);

                        System.IO.File.Delete(filePath);
                    }

                    employee.PhotoPath = ProcessUploadedFile(model);
                }

                var response = _employeeRepository.Update(employee);

                if (response != null && response.Id != 0)
                {
                    return RedirectToAction("Index");
                }
            }

            return View();
        }
        public IActionResult Delete(int id)
        {
            var employee = _employeeRepository.GetById(id);

            return View(employee);
        }

        public IActionResult DeleteShure(int id)
        {
            var employee = _employeeRepository.GetById(id);

            if (employee != null)
            {
                DeleteImage(employee.PhotoPath);

                var response = _employeeRepository.Delete(id);

                if (response != null && response.Id != 0)
                {
                    return RedirectToAction("Index");
                }

                return View(employee); 
            }
            return View("Index", "Home");
        }

        private void DeleteImage(string photoPath)
        {
            if (photoPath != null)
            {
                var filePath = Path.Combine(_webHostEnvironment.WebRootPath, "images", photoPath);
                System.IO.File.Delete(filePath);
            }
        }

        private string ProcessUploadedFile(EmployeeCreateViewModel model)
        {
            string uniqueFileName = null;

            if (model.Photo != null)
            {
                var uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images");
                uniqueFileName = $"{Guid.NewGuid().ToString()}_{model.Photo.FileName}";
                var filePath = Path.Combine(uploadsFolder, uniqueFileName);

                //wraps in using because cannot access filepath it is being used by another process
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    model.Photo.CopyTo(fileStream);
                }
            }

            return uniqueFileName;
        }
    }
}
