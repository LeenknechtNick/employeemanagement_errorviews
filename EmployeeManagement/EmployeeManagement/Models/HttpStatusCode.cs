﻿
namespace EmployeeManagement.Models
{
    public class HttpStatusCode
    {
        public string ErrorMessage { get; set; }
        public string Path { get; set; }   
        public string QS { get; set; }
    }
}
