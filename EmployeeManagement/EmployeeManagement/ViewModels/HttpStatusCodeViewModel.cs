﻿using EmployeeManagement.Models;

namespace EmployeeManagement.ViewModels
{
    public class HttpStatusCodeViewModel
    {
        public HttpStatusCode HttpStatusCode { get; set; }

    }
}
